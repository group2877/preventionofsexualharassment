# Prevention of Sexual Harassment

# 1. What kinds of behavior cause sexual harassment?

Any unwelcome behavior of a sexual nature that is pervasive and affects a person severely and can affect their working conditions and/or create a hostile work environment can be considered sexual harassment. They are broadly classified into 3:

- **Verbal Harassment**
- **Visual Harassment**
- **Physical Harassment**

## **1. Verbal Harassment**

Various forms of Verbal Harassment include:

- Comments about clothing
- A Person's Body
- Sexual or Gender-based jokes or remarks
- Requesting Sexual favors
- Repeatedly asking a person out
- Sexual Innuendos
- Threats
- Spreading rumors about a person's personal or sexual life

## **2. Visual Harassment**

The following objects of sexual nature can be considered visual harassment:

- Obscene Posters
- Drawings or Pictures
- Screensavers
- Cartoons
- Emails or Texts

## **3. Physical Harassment**

- Sexual Assault
- Impeding or Blocking movement
- Inappropriate touching(such as kissing, hugging, patting, stroking, rubbing, etc.)
- Sexual gesturing, leering, or staring

Each of these forms of sexual harassment can be further classified into two broader categories:

- **Quid pro quo**
- **Hostile Work Environment**

## **1. Quid Pro Quo**

It means '**this for that**' It happens when an employer or supervisor coerces a fellow employee into a sexual relationship or sexual act in disguise for job rewards or punishments. It includes offering the employee raises or promotions, threatening demotions, firing, or other penalties for not complying with the request.

## **2. Hostile Work Environment**

It occurs when employee behavior interferes with the work performance of another or creates an intimidating or offensive workplace environment. It makes the affected employee feel so uncomfortable that it affects their productivity.

# 2. What would you do in case you face or witness any incident or repeated incidents of such behavior?

If I face such an incident, I would first ask the person who has acted in that way to stop first. If the person does not stop themselves and continues with their way, I would report them to the supervisor and workers and call out the person.

If I witness such an incident, I would discourage the act and warn the employee/employer who is carrying out such an act and report them to the supervisor, if they repeat or fail to comply.

## References

- https://www.youtube.com/watch?v=Ue3BTGW3uRQ
- https://www.youtube.com/watch?v=u7e2c6v1oDs
